import Vue from 'vue'
import Router from 'vue-router'
import PopularMovies from '@/components/PopularMovies'
import LatestComponent from '@/components/LatestComponent'
import Favorites from '@/components/Favorites'
import Genres from '@/components/Genres'
import Overlay from '@/components/Overlay'

Vue.use(Router)

export default new Router({
  mode: "history",
  routes: [
    {
      path: '/',
      name: 'PopularMovies',
      component: PopularMovies
    },
    {
      path: '/latest',
      name: 'LatestComponent',
      component: LatestComponent
    },
    {
      path: '/favorites',
      name: 'Favorites',
      component: Favorites
    },
    {
      path: '/overlay',
      name: 'Overlay',
      component: Overlay
    },
    {
      path: '/genres',
      name: 'Genres',
      component: Genres
    },

  ]
})
