import Vue from "vue";
import App from "./App"
import router from "./router"
import "bootstrap/dist/css/bootstrap.min.css"
import "font-awesome/css/font-awesome.min.css"
import StarRating from "vue-star-rating"

Vue.component('star-rating', StarRating);

Vue.config.productionTip = false

new Vue({
  el: "#app",
  data: {
    isOpen: false,
  },
  methods: {
    toggle: () =>{
      this.isOpen = !this.isOpen
    }
  },
  router,
  render: h => h(App),
})
